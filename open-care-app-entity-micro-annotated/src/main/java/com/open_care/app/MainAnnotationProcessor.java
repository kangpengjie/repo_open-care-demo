package com.open_care.app;

import com.open_care.annotation_processor.sql.SQLAnnotationProcessor;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by kangpengjie on 2018/4/23.
 */
public class MainAnnotationProcessor {
    public static void main(String args[]) {
        if (args.length != 1) {
            throw new IllegalArgumentException("please config the target jar parameter in gradle file");
        }

        if (StringUtils.equals(args[0], "customer")) {
            new SQLAnnotationProcessor(new CustomerAnnotationContext()).manipulateJavaLibClasses();
            return;
        }

        if (StringUtils.equals(args[0], "customerBase")) {
            new SQLAnnotationProcessor(new CustomerNoAnnotationContext()).manipulateJavaLibClasses();
            return;
        }

        throw new UnsupportedOperationException("the argument " + args[0] + " doesn't be supported");
    }
}
