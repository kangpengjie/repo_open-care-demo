package com.open_care.app;

import com.open_care.core.BaseObject;
import com.open_care.jsonschema.JsonSchemaData;
import com.open_care.jsonschema.JsonSchemaDefinition;
import com.open_care.sys.*;

import java.util.ArrayList;
import java.util.List;

public class CustomerNoAnnotationContext extends CustomerAnnotationContext {
    @Override
    public boolean enableAnnotation() {
        return false;
    }

    @Override
    public String getTargetClassFilePath() {
        return "build/unpacked/customerBase";
    }

    @Override
    public List<String> getExcludeClasses() {
        List<String> result = new ArrayList<>();
        result.add(BaseObject.class.getName());
        result.add(JsonSchemaData.class.getName());
        result.add(JsonSchemaDefinition.class.getName());
        result.add(OCBase.class.getName());
        result.add(OcUser.class.getName());
        result.add(OcRole.class.getName());
        result.add(OcOrg.class.getName());
        result.add(OcContact.class.getName());
        return result;
    }
}
