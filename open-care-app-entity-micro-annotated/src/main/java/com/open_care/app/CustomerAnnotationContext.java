package com.open_care.app;

import com.google.gson.reflect.TypeToken;
import com.open_care.annotation_processor.annotation.impl.persistence.ColumnImpl;
import com.open_care.customer.crm.*;
import com.open_care.customer.market.OCDiscountRule;
import com.open_care.customer.market.OCProfitLevelRuntime;
import com.open_care.customer.market.OCProvideServiceProductsRule;
import com.open_care.customer.market.OCRuleRuntime;
import com.open_care.customer.party.OCOrganization;
import com.open_care.customer.party.OCParty;
import com.open_care.customer.party.OCPartyRole;
import com.open_care.customer.party.OCPerson;
import com.open_care.customer.visitRecord.OCCustomerReturnVisitRecord;
import com.open_care.customer.visitRecord.OCCustomerReturnVisitRecordSourceDetail;
import com.open_care.dto.OcFieldDTO;
import com.open_care.event.OCAppointmentProductAndResourcesInfo;
import com.open_care.event.OCRegisterAppointment;
import com.open_care.event.OCResourceAppointment;
import com.open_care.event.OCServiceProductAppointment;
import com.open_care.resource.*;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.math.BigDecimal;
import java.util.*;

public class CustomerAnnotationContext extends SqlAnnotationProcessorContext {
    @Override
    public String getTargetClassFilePath() {
        return "build/unpacked/Customer";
    }

    @Override
    public String[] getPackageNamesToScan() {
        return new String[]{"com.open_care.customer", "com.open_care.resource", "com.open_care.event", "com.open_care.sys", "com.open_care.jsonschema"};
    }

    @Override
    public List<Pair<Pair<Class, String>, Pair<Class, String>>> getOneToManyAndManyToOnePairList() {
        List<Pair<Pair<Class, String>, Pair<Class, String>>> result = new ArrayList<>();
        result.add(new ImmutablePair<>(new ImmutablePair<>(OCParty.class, "partyRoles"), new ImmutablePair<>(OCPartyRole.class, "party")));
        result.add(new ImmutablePair<>(new ImmutablePair<>(OCServiceProductAppointment.class, "productAppointmentAndResourcesInfos"),
                new ImmutablePair<>(OCAppointmentProductAndResourcesInfo.class, "serviceProductAppointment")));

        result.add(new ImmutablePair<>(new ImmutablePair<>(OCBusinessActivities.class, "leads"),
                new ImmutablePair<>(OCLead.class, "businessActivities")));
        result.add(new ImmutablePair<>(new ImmutablePair<>(OCEnterpriseCustomer.class, "opportunities"),
                new ImmutablePair<>(OCOpportunity.class, "customer")));
        result.add(new ImmutablePair<>(new ImmutablePair<>(OCCustomer.class, "contacts"),
                new ImmutablePair<>(OCContactPerson.class, "customer")));
        result.add(new ImmutablePair<>(new ImmutablePair<>(OCPayBack.class, "invoices"),
                new ImmutablePair<>(OCInvoice.class, "payBack")));
        result.add(new ImmutablePair<>(new ImmutablePair<>(OCCustomerReturnVisitRecord.class, "sourceDataDetailList"),
                new ImmutablePair<>(OCCustomerReturnVisitRecordSourceDetail.class, "customerReturnVisitRecord")));
        return result;
    }

    @Override
    public Map<Class, Triple<String, List<String>, List<String>>> getClassAndTableNameAndIndexNameAndUniqueNameListPairs() {
        Map<Class, Triple<String, List<String>, List<String>>> result = new HashMap<>();
        return result;
    }

    @Override
    public Map<Class, List<String>> getCustomUniqueColumns() {
        Map<Class, List<String>> result = new HashMap<>();
        result.put(OCPerson.class, Arrays.asList("certType,certNo"));
        return result;
    }

    @Override
    public List<Triple<Class, String, String>> getJoinTableNames() {
        List<Triple<Class, String, String>> result = new ArrayList<>();
        result.add(new ImmutableTriple<>(OCBusinessActivities.class, "leads", "OCBusinessActivitiesLeads"));
        result.add(new ImmutableTriple<>(OCContactPerson.class, "businessActivities", "OCContactPersonActivities"));
        result.add(new ImmutableTriple<>(OCCRMBaseObject.class, "crmObjectAssignLogs", "OCCRMBaseObjectAssignlogs"));
        result.add(new ImmutableTriple<>(OCCustomer.class, "customerTeams", "OCCustomerTeams"));
        result.add(new ImmutableTriple<>(OCEnterpriseCustomer.class, "businessActivities", "OCEnterpriseCustomerActivities"));

        result.add(new ImmutableTriple<>(OCEnterpriseCustomer.class, "contacts", "OCEnterpriseCustomerContacts"));
        result.add(new ImmutableTriple<>(OCEnterpriseCustomer.class, "contracts", "OCEnterpriseCustomerContracts"));
        result.add(new ImmutableTriple<>(OCEnterpriseCustomer.class, "opportunities", "OCEnterpriseCustomerOpportuni"));

        result.add(new ImmutableTriple<>(OCFollowUp.class, "contactPersons", "OCFollowUpContacts"));

        result.add(new ImmutableTriple<>(OCOpportunity.class, "businessActivities", "OCOpportunityActivities"));
        result.add(new ImmutableTriple<>(OCOpportunity.class, "competitorAnalysises", "OCOpportunityAnalysises"));
        result.add(new ImmutableTriple<>(OCOpportunity.class, "contactRoles", "OCOpportunityContactRoles"));

        result.add(new ImmutableTriple<>(OCParty.class, "partyContactMeches", "OCPartyContactMeches"));

        result.add(new ImmutableTriple<>(OCServiceProductAppointment.class, "productAppointmentAndResourcesInfos", "OCServiceProductAppointmentAndResources"));
        result.add(new ImmutableTriple<>(OCProvideServiceProductsRule.class, "serviceProductRuleInfoList", "ProvideServiceProduct_ruleInfoList"));
        return result;
    }

    @Override
    public Map<Class, List<OcFieldDTO>> getExtendFields() {
        Map<Class, List<OcFieldDTO>> result = new HashMap<>();

        List<OcFieldDTO> resourceGroupDTOS = new ArrayList<>();
        resourceGroupDTOS.add(new OcFieldDTO("medicalDepartmentInfo", String.class, "医疗科室信息"));
        result.put(OCResourceGroup.class, resourceGroupDTOS);

        List<OcFieldDTO> enterpriseCustomerDTOS = new ArrayList<>();
        enterpriseCustomerDTOS.add(new OcFieldDTO("mainContactPerson", OCContactPersonInfo.class, "主要联系人"));
        enterpriseCustomerDTOS.add(new OcFieldDTO("alternateContactPerson", OCContactPersonInfo.class, "备选联系人"));
        enterpriseCustomerDTOS.add(new OcFieldDTO("phone", String.class, "企业联系电话"));

        result.put(OCEnterpriseCustomer.class, enterpriseCustomerDTOS);

        List<OcFieldDTO> supplierDTOS = new ArrayList<>();
        supplierDTOS.add(new OcFieldDTO("mainContactPerson", OCContactPersonInfo.class, "主要联系人"));
        result.put(OCSupplier.class, supplierDTOS);

        List<OcFieldDTO> registrationAppointmentFieldDTOs = new ArrayList<>();
        registrationAppointmentFieldDTOs.add(new OcFieldDTO("sequenceNumber", Long.class, "序号"));
        result.put(OCRegisterAppointment.class, registrationAppointmentFieldDTOs);

        List<OcFieldDTO> serviceProductAppointmentFieldDTOs = new ArrayList<>();
        serviceProductAppointmentFieldDTOs.add(new OcFieldDTO("room", String.class, "房间"));
        serviceProductAppointmentFieldDTOs.add(new OcFieldDTO("reception", String.class, "接待人"));
        result.put(OCServiceProductAppointment.class, serviceProductAppointmentFieldDTOs);

        List<OcFieldDTO> resourceFieldDTOS = new ArrayList<>();
        resourceFieldDTOS.add(new OcFieldDTO("canAppointment", Boolean.class, "是否可被预约"));
        result.put(OCResource.class, resourceFieldDTOS);

        List<OcFieldDTO> projectQuotationDTOS = new ArrayList<>();
        projectQuotationDTOS.add(new OcFieldDTO("reportDeliveryMethod", Boolean.class, "报告递送方式"));
        projectQuotationDTOS.add(new OcFieldDTO("opportunity", String.class, "商机"));
        projectQuotationDTOS.add(new OcFieldDTO("processInstanceId", String.class, "流程实例ID"));
        //TODO 用扩展字段方式定义的Boolean类型 没有get方法(是is方法)，BeanUtils.copyProperties时无法复制属性
        //projectQuotationDTOS.add(new OcFieldDTO("draft", Boolean.class,"草稿"));

        result.put(OCProjectQuotation.class, projectQuotationDTOS);

        List<OcFieldDTO> paybackDTOS = new ArrayList<>();
        paybackDTOS.add(new OcFieldDTO("projectQuotation", OCProjectQuotation.class, "报价单"));
        result.put(OCPayBack.class, paybackDTOS);

        List<OcFieldDTO> followUpDTOs = new ArrayList<>();
        followUpDTOs.add(new OcFieldDTO("location", String.class, "位置"));
        result.put(OCFollowUp.class, followUpDTOs);

        List<OcFieldDTO> personalCustomerFieldDTOS = new ArrayList<>();
        personalCustomerFieldDTOS.add(new OcFieldDTO("openId", String.class, ""));
        personalCustomerFieldDTOS.add(new OcFieldDTO("wechatUserInfo", OCWechatUserInfo.class, ""));
        personalCustomerFieldDTOS.add(new OcFieldDTO("photo", String.class, ""));
        result.put(OCPersonalCustomer.class, personalCustomerFieldDTOS);

        List<OcFieldDTO> discountRuleFieldDTOS = new ArrayList<>();
        discountRuleFieldDTOS.add(new OcFieldDTO("productCategory", new TypeToken<List<String>>() {
        }.getType(), "产品类别"));
        result.put(OCDiscountRule.class, discountRuleFieldDTOS);

        List<OcFieldDTO> personFieldDTOS = new ArrayList<>();
        personFieldDTOS.add(new OcFieldDTO("phonetic", String.class, "姓名拼音"));
        personFieldDTOS.add(new OcFieldDTO("firstPhonetic", String.class, "拼音首字母"));
        result.put(OCPerson.class, personFieldDTOS);

        List<OcFieldDTO> organizationDTOS = new ArrayList<>();
        organizationDTOS.add(new OcFieldDTO("address", String.class, "单位地址"));
        result.put(OCOrganization.class, organizationDTOS);

        List<OcFieldDTO> opportunityDTOS = new ArrayList<>();
        opportunityDTOS.add(new OcFieldDTO("nian", String.class, "年度"));
        opportunityDTOS.add(new OcFieldDTO("signPersons", String.class, "签单人数"));
        result.put(OCOpportunity.class, opportunityDTOS);

        List<OcFieldDTO> contactPersonDTOS = new ArrayList<>();
        contactPersonDTOS.add(new OcFieldDTO("opportunity", OCOpportunity.class, "商机"));
        result.put(OCContactPerson.class, contactPersonDTOS);
        return result;
    }

    public List<Triple<Class, String, String>> getManyToManyFieldsForCollectionType() {
        List<Triple<Class, String, String>> result = new ArrayList<>();

        result.add(new ImmutableTriple<>(OCResourceAppointment.class, "resources", ""));

        result.add(new ImmutableTriple<>(OCResource.class, "resourceGroups", ""));
        result.add(new ImmutableTriple<>(OCResourceGroup.class, "resources", "resourceGroups"));
        return result;
    }

    public List<Pair<Class, String>> initialize() {
        List<Pair<Class, String>> result = new ArrayList<>();
        result.add(new ImmutablePair<>(OCResourceSchedule.class, "resources"));
        result.add(new ImmutablePair<>(OCResourceSchedule.class, "scheduleSetting"));
        result.add(new ImmutablePair<>(OCResourceSchedule.class, "resourceGroup"));

        result.add(new ImmutablePair<>(OCScheduleSetting.class, "resources"));
        result.add(new ImmutablePair<>(OCScheduleSetting.class, "resourceGroup"));

        result.add(new ImmutablePair<>(OCResource.class, "resourceCategory"));
        result.add(new ImmutablePair<>(OCResource.class, "resourceGroups"));

        result.add(new ImmutablePair<>(OCResourceGroup.class, "resources"));

        result.add(new ImmutablePair<>(OCResourceAppointment.class, "resources"));
        result.add(new ImmutablePair<>(OCRegisterAppointment.class, "resourceGroup"));

        result.add(new ImmutablePair<>(OCResourceGroupAssocResource.class, "resource"));
        result.add(new ImmutablePair<>(OCResourceGroupAssocResource.class, "resourceGroup"));

        result.add(new ImmutablePair<>(OCProjectQuotation.class, "customer"));
        result.add(new ImmutablePair<>(OCProjectQuotation.class, "staffs"));

        result.add(new ImmutablePair<>(OCRuleRuntime.class, "partyRole"));
        result.add(new ImmutablePair<>(OCProfitLevelRuntime.class, "partyRole"));

        result.add(new ImmutablePair<>(OCAppointmentProductAndResourcesInfo.class, "serviceProductAppointment"));

        //result.add(new ImmutablePair<>(OCOpportunity.class, "customer"));

        result.add(new ImmutablePair<>(OCInvoice.class, "payBack"));

        result.add(new ImmutablePair<>(OCPayBack.class, "customer"));
        result.add(new ImmutablePair<>(OCPayBack.class, "contract"));
        result.add(new ImmutablePair<>(OCPayBack.class, "projectQuotation"));

        result.add(new ImmutablePair<>(OCContract.class, "partyRole"));
        result.add(new ImmutablePair<>(OCContract.class, "opportunity"));
        result.add(new ImmutablePair<>(OCContract.class, "quotation"));
        result.add(new ImmutablePair<>(OCContract.class, "parent"));

        result.add(new ImmutablePair<>(OCFollowUp.class, "customer"));

        result.add(new ImmutablePair<>(OCContactPerson.class, "customer"));

        result.add(new ImmutablePair<>(OCSaleProject.class, "customer"));
        result.add(new ImmutablePair<>(OCSaleProject.class, "opportunity"));

        //result.add(new ImmutablePair<>(OCPartyRole.class, "party"));
        return result;
    }

    @Override
    public List<Triple<Class, String, ColumnImpl>> getCustomFields() {
        List<Triple<Class, String, ColumnImpl>> result = new ArrayList<>();
        result.add(new ImmutableTriple<>(OCPersonalCustomer.class, "photo",new ColumnImpl("","mediumblob")));
        result.addAll(super.getCustomFields());
        return result;
    }
}
