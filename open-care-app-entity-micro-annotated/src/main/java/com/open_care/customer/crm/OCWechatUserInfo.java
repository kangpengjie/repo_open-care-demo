package com.open_care.customer.crm;

import com.open_care.sys.OCBase;
import lombok.Data;

@Data
public class OCWechatUserInfo extends OCBase {
    private String avatarUrl;
    private String city;
    private String country;
    private String gender;
    private String language;
    private String nickName;
    private String province;
}
