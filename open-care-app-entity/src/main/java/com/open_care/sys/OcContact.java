package com.open_care.sys;

import com.open_care.core.BaseJsonObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by kangpengjie on 2018/4/17.
 */
@Getter
@Setter
public class OcContact extends BaseJsonObject {
    private String type;
    private String value;
}
