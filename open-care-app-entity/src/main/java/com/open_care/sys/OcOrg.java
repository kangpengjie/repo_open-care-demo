package com.open_care.sys;

import com.open_care.annotation.OcClass;
import com.open_care.annotation.OcColumn;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@OcClass(title = "机构")
public class OcOrg extends OCBase {
    private String orgName;
    private String orgNo;
    private OcOrg parentOrg;
    private List<OcUser> users;
}
