package com.open_care.sys;

import com.open_care.annotation.OcClass;
import com.open_care.annotation.OcColumn;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@OcClass(title = "角色")
public class OcRole extends OCBase {
    private String key;
    private String name;
    @OcColumn(title = "医疗角色")
    private Boolean medicalRole;
}
