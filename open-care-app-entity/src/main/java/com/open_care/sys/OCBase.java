package com.open_care.sys;

import com.fasterxml.jackson.annotation.*;
import com.google.gson.JsonElement;
import com.open_care.annotation.OcCreatedBy;
import com.open_care.annotation.OcUpdatedBy;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Transient;
import java.util.Date;

@Data
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "ocId")
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class OCBase {
    @JsonIgnore
    private JsonElement attributes;
    @CreationTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    Date created;
    @OcCreatedBy
    String createdBy;
    @UpdateTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    Date updated;
    @OcUpdatedBy
    String updatedBy;
    Boolean active = true;
    @Transient
    String entityName = this.getClass().getName();
}

