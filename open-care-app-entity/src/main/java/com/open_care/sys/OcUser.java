package com.open_care.sys;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.gson.annotations.JsonAdapter;
import com.open_care.adapter.OCDateTypeAdapter;
import com.open_care.annotation.ComponentStyle;
import com.open_care.annotation.OcClass;
import com.open_care.annotation.OcColumn;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@OcClass(title = "用户")
public class OcUser extends OCBase { //用户
    @OcColumn(title = "用户名")
    private String username;
    @OcColumn(title = "姓名")
    private String name;
    @OcColumn(title = "口令")
    private String password;
    @OcColumn(title = "角色")
    private List<OcRole> roles;
    @OcColumn(title = "contacts")
    private List<OcContact> contacts;
    @OcColumn(title = "所属机构", style = ComponentStyle.SELECT)
    private OcOrg ownOrg;
    @OcColumn(title = "状态")
    private String status;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @JsonAdapter(OCDateTypeAdapter.class)
    private Date birthday;
}
