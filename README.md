该文档主要描述应用子工程概要和开发步骤。

子工程：
1、open-care-app-entity
定义应用所需要的Entity
2、open-care-app-entity-annotated
为应用中Entity添加注解、断开级联关系等，完成维护后使用gradle中copyAnnotatedJar Task build jar包
3、open-care-app-entity-micro-annotated
本应用所需要的微服务
3.1、customer：客户微服务
3.2、medicalMicro：医疗微服务
3.3、product：产品微服务
4、open-care-app-server
业务逻辑实现
需要执行gradle clean build 将所需要的jar进行生成


开发步骤：
1、定义Entity对应的DTO
1.1、如果是微服务Entity CRUD操作需要按照如下格式定义
public class BaseAllergenDTO extends OCBaseAllergen implements IBaseDTO {
}
BaseAllergenDTO：自定义DTO名称
OCBaseAllergen：该DTO对应的Entity
1.2、如果是应用相关DTO则按照如下格式定义
public class UserDTO extends BaseDTO {
}
2、定义JAVA使用DTO无特殊要求
3、执行CRUD操作时需要调用该Entity/DTO 对应微服务的CRUD方法
注意：
使用subform组件时_entityName必须和subform绑定Entity name保持一致
使用table组件时，返回data list中必须有_rowid字段

具体参考wiki：
https://wiki.open-care.com/display/CRMCAR/How+to+start+develop---for+beginner

