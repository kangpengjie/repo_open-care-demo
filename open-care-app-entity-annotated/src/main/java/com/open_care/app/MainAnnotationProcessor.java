package com.open_care.app;

import com.open_care.annotation_processor.AnnotationProcessorContext;
import com.open_care.annotation_processor.sql.SQLAnnotationProcessor;

/**
 * Created by kangpengjie on 2018/4/23.
 */
public class MainAnnotationProcessor {
    public static void main(String args[]){
        AnnotationProcessorContext annotationProcessorContext = new SqlAnnotationProcessorContext();
        SQLAnnotationProcessor sqlAnnotationProcessor = new SQLAnnotationProcessor(annotationProcessorContext);
        sqlAnnotationProcessor.manipulateJavaLibClasses();

    }
}
