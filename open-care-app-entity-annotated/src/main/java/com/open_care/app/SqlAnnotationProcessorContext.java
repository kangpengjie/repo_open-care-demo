package com.open_care.app;

import com.google.gson.JsonElement;
import com.open_care.annotation_processor.AnnotationProcessorContextForOneClass;
import com.open_care.annotation_processor.ClassTypeForEntityForPersistence;
import com.open_care.annotation_processor.DefaultAnnotationProcessorContext;
import com.open_care.annotation_processor.annotation.impl.persistence.ColumnImpl;
import com.open_care.core.BaseJsonObject;
import com.open_care.core.BaseObject;
import com.open_care.core.IBaseObject;
import com.open_care.dto.OcOperationDTO;
import com.open_care.jsonschema.JsonSchemaObject;
import com.open_care.sys.OCBase;
import com.open_care.sys.OcOrg;
import com.open_care.sys.OcRole;
import com.open_care.sys.OcUser;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by kangpengjie on 2018/4/23.
 */
public class SqlAnnotationProcessorContext extends DefaultAnnotationProcessorContext {
    @Override
    public String getTargetClassFilePath() {
        return "build/unpacked/dist";
    }

    @Override
    public String[] getPackageNamesToScan() {
        return new String[]{"com.open_care"};
    }

    @Override
    public List<AnnotationProcessorContextForOneClass> getBaseClasses() {
        return Arrays.asList(
                new AnnotationProcessorContextForOneClass(BaseObject.class),
                new AnnotationProcessorContextForOneClass(BaseJsonObject.class),
                new AnnotationProcessorContextForOneClass(OCBase.class, IBaseObject.class)
        );
    }

    @Override
    public String getJsonType() {
        return "json";
    }

    @Override
    public boolean needDefineElementAutoId() {
        return false;
    }

    @Override
    public ClassTypeForEntityForPersistence getClassType(Class<?> clz) {
        if (clz == null) {
            return null;
        }

        if (BaseJsonObject.class.isAssignableFrom(clz) || JsonSchemaObject.class.isAssignableFrom(clz)
                || JsonElement.class.isAssignableFrom(clz)) {
            return ClassTypeForEntityForPersistence.Json;
        }

        if (BaseObject.class.isAssignableFrom(clz) || OCBase.class.isAssignableFrom(clz)) {
            return ClassTypeForEntityForPersistence.Entity;
        }

        if (clz.isArray() || clz.isEnum() || ClassUtils.isPrimitiveOrWrapper(clz)
                || String.class.isAssignableFrom(clz) || Date.class.isAssignableFrom(clz)
                || BigDecimal.class.isAssignableFrom(clz)) {
            return ClassTypeForEntityForPersistence.Basic;
        }

        if (clz.equals(Object.class) || clz.equals(Comparable.class)) {
            return ClassTypeForEntityForPersistence.Uncertain;
        }

        return ClassTypeForEntityForPersistence.NoEntity;
    }

    @Override
    public boolean needBytecodeEnhancement() {
        return true;
    }

    @Override
    public String getAutoAssignedGenerator() {
        return null;
    }

    @Override
    public String getAutoAssignedStrategy() {
        return null;
    }

    @Override
    public boolean enableJoinTablePrimaryKeyForListTypeField() {
        return true;
    }

    @Override
    public boolean enableJSONSupport() {
        return true;
    }

    @Override
    public List<Pair<Class, String>> getAssociationsForNotOrphanRemoval() {
        return initialize();
    }

    @Override
    public List<Pair<Class, String>> getAssociationsForNotCascade() {
        return initialize();
    }

    @Override
    public List<Triple<Class, String, Class>> getTargetClassTypeForUncertainField() {
        return null;
    }

    @Override
    public Class getDefaultTargetClassTypeForUncertainField() {
        return null;
    }

    @Override
    public Map<Class, Triple<String, List<String>, List<String>>> getClassAndTableNameAndIndexNameAndUniqueNameListPairs() {
        Map<Class, Triple<String, List<String>, List<String>>> result = new HashMap<>();

        return result;
    }

    @Override
    public boolean needMakeLobAnnotation(Class aClass, String s) {
        return false;
    }

    @Override
    public List<Pair<Pair<Class, String>, Pair<Class, String>>> getOneToManyAndManyToOnePairList() {
        List<Pair<Pair<Class, String>, Pair<Class, String>>> result = new ArrayList<>();

        result.add(new ImmutablePair<>(new ImmutablePair<>(OcOrg.class, "users"),
                new ImmutablePair<>(OcUser.class, "ownOrg")));

        return result;
    }

    public List<Triple<Class, String, String>> getManyToManyFieldsForCollectionType() {
        List<Triple<Class, String, String>> result = new ArrayList<>();

        result.add(new ImmutableTriple<>(OcUser.class, "roles", ""));
        return result;
    }

    @Override
    public boolean isJsonClass(Class var1) {
        return false;
    }

    ;

    @Override
    public boolean isEntityClass(Class var1) {
        return false;
    }

    public List<Pair<Class, String>> initialize() {
        List<Pair<Class, String>> result = new ArrayList<>();

        result.add(new ImmutablePair<>(OcUser.class, "ownOrg"));
        result.add(new ImmutablePair<>(OcUser.class, "superior"));
        result.add(new ImmutablePair<>(OcUser.class, "roles"));

        return result;
    }

    @Override
    public String getIdentifierProperty() {
        return "ocId";
    }

    @Override
    public OcOperationDTO getRecordOperationAndTime() {
        return new OcOperationDTO();
        //return new OcOperationDTO("created", "createdBy", "updated", "updatedBy");
    }

    @Override
    public List<Triple<Class, String, ColumnImpl>> getCustomFields() {
        List<Triple<Class, String, ColumnImpl>> result = new ArrayList<>();

        result.add(new ImmutableTriple<>(OcRole.class, "key", new ColumnImpl("oc_key", 255)));
        return result;
    }

    @Override
    /**
     * 重新生成中间表表名
     * Left：Entity对应的class
     * Middle：Entity关联的Entity field
     * Right：新中间表名
     */
    public List<Triple<Class, String, String>> getJoinTableNames() {
        List<Triple<Class, String, String>> result = new ArrayList<>();

        return result;
    }
}
