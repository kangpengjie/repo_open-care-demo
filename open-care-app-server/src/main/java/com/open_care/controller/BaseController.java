package com.open_care.controller;

import com.open_care.annotation.OCLog;
import com.open_care.api.client.CustomerRemoteService;
import com.open_care.api.common.delete.DeleteParamRequest;
import com.open_care.api.common.dto.EnableOrDisableDataParamRequestDTO;
import com.open_care.api.common.dto.OcResponse;
import com.open_care.api.common.dto.RefParamDTO;
import com.open_care.api.common.dto.UpdateTreeDTO;
import com.open_care.api.common.dto.crud.GetRequestDTO;
import com.open_care.api.common.dto.crud.QueryRequestDTO;
import com.open_care.api.common.dto.crud.SaveRequestDTO;
import com.open_care.customer.crm.*;
import com.open_care.customer.party.OCPerson;
import com.open_care.dto.sys.PersonalCustomerDTO;
import com.open_care.dto.sys.UserDTO;
import com.open_care.service.AppService;
import com.open_care.service.CustomerService;
import com.open_care.service.UserService;
import com.open_care.sys.OcUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.PostConstruct;
import java.util.*;

public class BaseController extends AppBaseController {
    @Autowired
    AppService appService;

    @Autowired
    UserService userService;

    @Autowired
    CustomerService customerService;

    @PostConstruct
    public void init() {
        getOperationMap.put(UserDTO.class.getName(),(((getRequestDTO, id) -> userService.getOne(getRequestDTO, OcUser.class.getName(), id))));
        saveOperationMap.put(UserDTO.class.getName(), (saveParamRequest) -> userService.save(saveParamRequest));
        queryOperationMap.put(UserDTO.class.getName(), (queryRequestDTO) -> userService.query(queryRequestDTO));

        saveOperationMap.put(PersonalCustomerDTO.class.getName(), (saveParamRequest) -> customerService.save(saveParamRequest));
        getOperationMap.put(PersonalCustomerDTO.class.getName(),(((getRequestDTO, id) -> customerService.getOne(getRequestDTO, OcUser.class.getName(), id))));
        queryOperationMap.put(PersonalCustomerDTO.class.getName(), (queryRequestDTO) -> customerService.query(queryRequestDTO));

        entityAndRemoteServiceMap.put(OCPerson.class.getName(), () -> appService.getCustomerService());
    }

    @Override
    public OcResponse query(@RequestBody QueryRequestDTO queryParamRequest) {
        return super.query(queryParamRequest);
    }

    @Override
    public OcResponse save(@RequestBody SaveRequestDTO saveParamRequest) {
        return super.save(saveParamRequest);
    }

    @Override
    public OcResponse update(@PathVariable("id") String id, @RequestBody SaveRequestDTO saveParamRequest) {
        return super.update(id, saveParamRequest);
    }

    @Override
    public OcResponse get(@RequestBody GetRequestDTO queryParamRequest, @PathVariable("entityName") String entityName, @PathVariable("id") String id) {
        return super.get(queryParamRequest, entityName, id);
    }

    @Override
    public OcResponse updateTree(@RequestBody UpdateTreeDTO updateTreeDTO) {
        return super.updateTree(updateTreeDTO);
    }

    @Override
    public OcResponse getChangedEntity(@RequestBody List<String> entityNames) {
        return super.getChangedEntity(entityNames);
    }

    @Override
    public List<Map<String, Object>> getValueOptionByEntity(@RequestBody RefParamDTO refParamDTO) {
        return super.getValueOptionByEntity(refParamDTO);
    }

    @Override
    OcResponse enableOrDisableData(@RequestBody EnableOrDisableDataParamRequestDTO enableOrDisableDataParamRequestDTO) {
        return super.enableOrDisableData(enableOrDisableDataParamRequestDTO);
    }

    @OCLog(description = "删除")
    @Override
    public OcResponse delete(@RequestBody DeleteParamRequest deleteParamRequest) {
        return super.delete(deleteParamRequest);
    }
}