package com.open_care.config;

import com.open_care.baseservice.BaseObjectActiveGetter;
import com.open_care.baseservice.BaseObjectIdGetter;
import com.open_care.baseservice.CustomBaseObjectActiveGetter;
import com.open_care.baseservice.CustomBaseObjectIdGetter;
import com.open_care.controller.BaseController;
import com.open_care.log.FileLogService;
import com.open_care.log.LogService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;


@Configuration
public class MyConfig {
    @Bean(name = "OpenCareBaseController")
    public BaseController baseController() {
        return new BaseController();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public BaseObjectIdGetter getBaseObjectIdGetter() {
        return new CustomBaseObjectIdGetter();
    }

    @Bean
    public BaseObjectActiveGetter getBaseObjectActiveGetter() {
        return new CustomBaseObjectActiveGetter();
    }

    @Bean
    public LogService logService() {
        return new FileLogService();
    }

}
