package com.open_care.config;

import com.open_care.context.AppInfo;
import com.open_care.context.AppInfoContext;
import com.open_care.context.UserInfo;
import com.open_care.context.UserInfoContext;
import org.springframework.core.task.TaskDecorator;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

public class ContextDecorator  implements TaskDecorator {
    @Override
    public Runnable decorate(Runnable runnable) {
        RequestAttributes context = RequestContextHolder.currentRequestAttributes();
        UserInfo userInfo = UserInfoContext.getUser();
        AppInfo appInfo = AppInfoContext.getApp();
        return () -> {
            try {
                RequestContextHolder.setRequestAttributes(context);
                UserInfoContext.setUser(userInfo);
                AppInfoContext.setApp(appInfo);
                runnable.run();
            } finally {
                RequestContextHolder.resetRequestAttributes();
            }
        };
    }
}
