package com.open_care.dto.sys;

import com.open_care.annotation.OcClass;
import lombok.Getter;
import lombok.Setter;

/**
 * @author:lee
 * @Description:
 * @Date:2019-10-09 11:15
 * @Modified By:
 */
@Getter
@Setter
@OcClass(title = "角色DTO")
public class RoleDTO  {
}
