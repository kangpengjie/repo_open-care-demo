package com.open_care.dto.sys;

import com.open_care.annotation.OcClass;
import com.open_care.sys.OcUser;
import lombok.Data;

import java.util.ArrayList;

@Data
@OcClass(title = "用户DTO")
public class UserDTO extends OcUser {
    private String age;
}
