package com.open_care.dto.sys;

import com.open_care.customer.crm.OCPersonalCustomer;
import com.open_care.customer.party.OCPerson;
import lombok.Data;

@Data
public class PersonalCustomerDTO extends OCPersonalCustomer {
    private OCPerson person;
}
