package com.open_care;

import com.open_care.feign.client.FeignConfiguration;
import com.open_care.repository.BaseRepositoryImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableDiscoveryClient
@EnableJpaRepositories(
        repositoryBaseClass = BaseRepositoryImpl.class
)
@EnableFeignClients(
        basePackages = {"com.open_care.api.client"}
)
@EnableSwagger2
@Import({FeignConfiguration.class})
@EnableScheduling
@EnableAsync
public class Application {
    public static void main(String[] args) {
        try {
            SpringApplication app = new SpringApplication(Application.class);
            app.run(args);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}