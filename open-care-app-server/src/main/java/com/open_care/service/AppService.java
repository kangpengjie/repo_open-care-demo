package com.open_care.service;

import com.open_care.api.common.service.BaseService;
import com.open_care.baseservice.BaseOperationService;
import com.open_care.context.UserInfoContext;
import com.open_care.repository.UserRepository;
import com.open_care.response.ResponseHandler;
import com.open_care.sys.OcOrg;
import com.open_care.sys.OcUser;
import com.open_care.util.RemoteServiceUtil;
import com.open_care.util.RestfulAPIUtil;
import com.open_care.util.ServiceUtil;
import com.open_care.util.json.JsonConverter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static com.open_care.constant.MicroServiceName.*;

@Service
public class AppService {
    public final static String ENTITY_ENABLE_STATUS = "enable";
    public final static String ENTITY_DISABLE_STATUS = "disable";
    public final static String ENTITY_OBJECT_ID = "ocId";
    public final static String ENTITY_NAME_FIELD = "entityName";
    public final static String BUSINESS_ENTITY_STATUS_FIELD_NAME = "status";
    public final static String ENTITY_ACTIVE_FIELD_NAME = "active";

    @Autowired
    protected DiscoveryClient discoveryClient;

    @Autowired
    protected ResponseHandler responseHandler;

    @Autowired
    protected BaseOperationService baseOperationService;

    @Autowired
    protected JsonConverter jsonConverter;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RemoteServiceUtil remoteServiceUtil;

    @Value("${web.filePath}")
    public String webFilePath;

    @Autowired
    public HttpServletRequest request;

    private Logger logger = LoggerFactory.getLogger(AppService.class);

    public BaseService getCustomerService() {
        return ServiceUtil.getBaseService(discoveryClient, CUSTOMER_SERVICE);
    }

    public OcUser getCurrentUser() {
        if (Objects.isNull(UserInfoContext.getUser())) {
            throw new IllegalStateException("无法从用户信息上下文中获取用户，请联系系统管理员");
        }

        if (userRepository.existsById(UserInfoContext.getUser().getUserId())) {
            return userRepository.getOne(UserInfoContext.getUser().getUserId());
        }

        throw new IllegalStateException("无法获取当前登录系统用户，请联系系统管理员");
    }

    public String getCurrentUserId() {
        return getCurrentUser().getOcId();
    }

    public OcOrg getCurrentOrg() {
        return getCurrentUser().getOwnOrg();
    }

    public String getNameByUserId(String userId) {
        if (StringUtils.isEmpty(userId)) {
            return null;
        }

        if (userRepository.existsById(userId)) {
            return userRepository.getOne(userId).getName();
        }
        return null;
    }
}

