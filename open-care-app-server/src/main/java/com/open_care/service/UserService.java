package com.open_care.service;

import com.open_care.api.common.dto.OcResponse;
import com.open_care.api.common.dto.crud.GetRequestDTO;
import com.open_care.api.common.dto.crud.QueryRequestDTO;
import com.open_care.api.common.dto.crud.SaveRequestDTO;
import com.open_care.api.common.edit.EditDataResponse;
import com.open_care.api.common.query.QueryDataResponse;
import com.open_care.dto.sys.UserDTO;
import com.open_care.persist.SchemaPersist;
import com.open_care.query.SchemaQuery;
import com.open_care.query.SimpleQuery;
import com.open_care.response.ResponseHandler;
import com.open_care.sys.OcUser;
import com.open_care.util.*;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class UserService extends AppService {
    @Autowired
    SchemaPersist schemaPersist;

    @Autowired
    SimpleQuery simpleQuery;

    @Autowired
    EntityManager entityManager;

    @Autowired
    SchemaQuery schemaQuery;

    @Autowired
    ResponseHandler responseHandler;

    @Transactional
    public OcResponse save(SaveRequestDTO saveParamRequest) {
        Map dtoRequestMap = GraphqlUtil.recursiveMapWithPoundSign(saveParamRequest.getData());
        UserDTO userDTO = GsonUtils.mapToObject(dtoRequestMap, UserDTO.class);

        OcUser user = baseOperationService.saveEntityObject(dtoRequestMap, OcUser.class, "");
        return responseHandler.handleSaveResponse(saveParamRequest, convertUserToUserDTO(user));
    }

    public OcResponse getOne(GetRequestDTO getRequestDTO, String entityName, String id) {
        OcUser user = baseOperationService.getEntityObject(id, OcUser.class, "ownOrg");
        return responseHandler.handleGetResponse(getRequestDTO, convertUserToUserDTO(user));
    }

    public OcResponse query(QueryRequestDTO queryRequestDTO) {
        queryRequestDTO.setEntityName(OcUser.class.getName());
        List<OcUser> userList = baseOperationService.queryEntityObject(queryRequestDTO, OcUser.class);
        return responseHandler.handleQueryResponse(queryRequestDTO, userList.stream().map(
                iter -> convertUserToUserDTO(iter)).collect(Collectors.toList()));
    }

    public UserDTO convertUserToUserDTO(OcUser user) {
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(user, userDTO);
        if(userDTO.getBirthday() != null) {
            userDTO.setAge(CommonUtils.getAgeStrFromBirthDate(userDTO.getBirthday()));
        }
        return userDTO;
    }
}
