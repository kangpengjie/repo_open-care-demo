package com.open_care.service;

import com.open_care.api.common.dto.OcResponse;
import com.open_care.api.common.dto.crud.GetRequestDTO;
import com.open_care.api.common.dto.crud.QueryRequestDTO;
import com.open_care.api.common.dto.crud.SaveRequestDTO;
import com.open_care.api.common.edit.EditDataResponse;
import com.open_care.api.common.query.QueryDataResponse;
import com.open_care.constant.GlobalConstant;
import com.open_care.customer.crm.OCPersonalCustomer;
import com.open_care.customer.party.OCPerson;
import com.open_care.dto.sys.PersonalCustomerDTO;
import com.open_care.dto.sys.UserDTO;
import com.open_care.query.SimpleQuery;
import com.open_care.sys.OcUser;
import com.open_care.util.GraphqlUtil;
import com.open_care.util.GsonUtils;
import com.open_care.util.MapUtil;
import com.open_care.util.ResponseUtil;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.ws.Action;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CustomerService extends AppService {
    @Autowired
    SimpleQuery simpleQuery;

    @Transactional
    public OcResponse save(SaveRequestDTO saveParamRequest) {
        Map dtoRequestMap = GraphqlUtil.recursiveMapWithPoundSign(saveParamRequest.getData());
        PersonalCustomerDTO personalCustomerDTO = GsonUtils.mapToObject(dtoRequestMap, PersonalCustomerDTO.class);

        MapUtil.replaceValueByKeyIfExisted(dtoRequestMap, "person", "party");
        MapUtil.setEntityNameForMapObject(dtoRequestMap, "party", OCPerson.class);
        dtoRequestMap.remove("person");

        OCPersonalCustomer personalCustomer = baseOperationService.saveEntityObject(
                getCustomerService(), OCPersonalCustomer.class, dtoRequestMap);
        return responseHandler.handleSaveResponse(saveParamRequest, convertToPersonalCustomerDTO(personalCustomer));
    }

    private PersonalCustomerDTO convertToPersonalCustomerDTO(OCPersonalCustomer personalCustomer) {
        PersonalCustomerDTO personalCustomerDTO = new PersonalCustomerDTO();
        BeanUtils.copyProperties(personalCustomer, personalCustomerDTO);
        personalCustomerDTO.setPerson((OCPerson) personalCustomer.getParty());
        personalCustomerDTO.setParty(null);

        return personalCustomerDTO;
    }

    public OcResponse getOne(GetRequestDTO getRequestDTO, String entityName, String id) {
        OCPersonalCustomer personalCustomer = baseOperationService.getEntityObject(
                getCustomerService(), id, OCPersonalCustomer.class, "");
        return responseHandler.handleGetResponse(getRequestDTO, convertToPersonalCustomerDTO(personalCustomer));
    }

    public OcResponse query(QueryRequestDTO queryRequestDTO) {
        queryRequestDTO.setEntityName(OCPersonalCustomer.class.getName());
        List<OCPersonalCustomer> personalCustomers =
                baseOperationService.queryAllEntityObjects(getCustomerService(), OCPersonalCustomer.class,
                        queryRequestDTO.getFilters(), "");

        List<PersonalCustomerDTO> personalCustomerDTOS =  personalCustomers.stream().map(
                iter -> convertToPersonalCustomerDTO(iter)).collect(Collectors.toList());
        return responseHandler.handleQueryResponse(queryRequestDTO, personalCustomerDTOS);
    }

    public OcResponse updatePersonalCustomer(String id) {
        OCPersonalCustomer personalCustomer = baseOperationService.getEntityObject(
                getCustomerService(), id, OCPersonalCustomer.class, GlobalConstant.EAGER_ROOT);

        personalCustomer.setCustomerName("test1");
        personalCustomer.setCustomerNo("0001");

        baseOperationService.saveEntityObject(getCustomerService(), OCPersonalCustomer.class, personalCustomer,"");
        return ResponseUtil.getResponseSuccess("成功");
    }
}
