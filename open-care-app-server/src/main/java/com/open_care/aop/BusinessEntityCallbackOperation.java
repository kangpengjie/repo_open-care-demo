package com.open_care.aop;

import org.springframework.stereotype.Service;

@Service
public class BusinessEntityCallbackOperation extends BusinessEntityOperationHook {
    public BusinessEntityCallbackOperation() {
        preSaveOperationMap.clear();
        postSaveOperationMap.clear();
    }
}
