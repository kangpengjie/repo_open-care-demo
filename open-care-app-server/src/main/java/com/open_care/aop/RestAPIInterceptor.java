package com.open_care.aop;

import com.open_care.security.token.JWTWso2TokenStore;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Component
public class RestAPIInterceptor implements HandlerInterceptor {
    @Autowired
    TokenStore tokenStore;

    @Value("${permitPattern}")
    private String permitPattern;

    protected static final Logger logger = LoggerFactory.getLogger(RestAPIInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //打印类名
        System.out.println(((HandlerMethod) handler).getBean().getClass().getName());
        //打印方法名
        System.out.println(((HandlerMethod) handler).getMethod().getName());
        logger.error("request url:" + request.getRequestURI());
        request.setAttribute("startTime", System.currentTimeMillis());
        request.setAttribute("traceId", UUID.randomUUID().toString());

        if (StringUtils.isNotEmpty(permitPattern)) {
            return true;
        }

        String token = request.getHeader("Authorization");
        if (StringUtils.isEmpty(token)) {
            throw new IllegalStateException("请求无效，请检查");
        }
        if (token.startsWith("Bearer")) {
            token = token.substring(7);
        }
        OAuth2AccessToken oAuth2AccessToken = ((JWTWso2TokenStore) tokenStore).readAccessToken(token);
        if (oAuth2AccessToken.isExpired()) {
            throw new IllegalStateException("请求无效，请检查");
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object o, ModelAndView modelAndView) throws Exception {
        Long start = (Long) request.getAttribute("startTime");
        logger.error(request.getRequestURI() + " postHandle 耗时：" + (System.currentTimeMillis() - start));
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object o, Exception ex) throws Exception {
        Long start = (Long) request.getAttribute("startTime");
        logger.error(request.getRequestURI() + " afterCompletion 耗时：" + (System.currentTimeMillis() - start));
    }
}
