package com.open_care.aop;

import org.springframework.stereotype.Service;

@Service
public class OCCustomBusinessInterceptor extends OCBusinessInterceptor {

    @Override
    public void preSave(Object entity) {
        super.preSave(entity);
    }
}
